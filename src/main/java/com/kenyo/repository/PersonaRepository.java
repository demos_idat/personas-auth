package com.kenyo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kenyo.domain.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Long> {
	
	Persona findOneByEmail(String email);

}
