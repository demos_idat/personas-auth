package com.kenyo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kenyo.domain.Persona;
import com.kenyo.service.PersonaService;

@RestController
@RequestMapping("/api/personas")
public class PersonaController {
	
	@Autowired
	private PersonaService service;
	
	@PreAuthorize("hasAuthority('USER')")
	@GetMapping
	public ResponseEntity<List<Persona>> listar() {
		return new ResponseEntity<>(service.listar(), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping
	public ResponseEntity<Persona> registrar(@RequestBody Persona persona) {
		return new ResponseEntity<>(service.registrar(persona), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@PutMapping
	public ResponseEntity<Persona> modificar(@RequestBody Persona persona) {
		return new ResponseEntity<>(service.registrar(persona), HttpStatus.OK);
	}
	
	//http://localhost:8080/auth/oauth/authorize?client_id=personaapp&response_type=code&scope=read_profile_info

}
