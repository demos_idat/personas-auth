package com.kenyo.service;

import java.util.List;

import com.kenyo.domain.Persona;

public interface PersonaService {
	
	Persona registrar(Persona persona);
	
	Persona modificar(Persona persona);
	
	
	List<Persona> listar();

}
