package com.kenyo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kenyo.domain.Persona;
import com.kenyo.repository.PersonaRepository;

@Service("userDetailsService")
public class UserSecurityServiceImpl implements UserDetailsService {
	
	@Autowired
	private PersonaRepository repository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Persona persona = repository.findOneByEmail(email);
		if(persona == null)
			throw new BadCredentialsException("No existe usuario");
		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority(persona.getRol().getNombre()));
		return new User(email, persona.getPassword(), roles);
	}

}
