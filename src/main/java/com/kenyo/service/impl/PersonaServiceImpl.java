package com.kenyo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.kenyo.domain.Persona;
import com.kenyo.repository.PersonaRepository;
import com.kenyo.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {
	
	@Autowired
	private PersonaRepository repository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Persona registrar(Persona persona) {
		persona.setPassword(passwordEncoder.encode(persona.getPassword()));
		return repository.save(persona);
	}

	@Override
	public Persona modificar(Persona persona) {
		persona.setPassword(passwordEncoder.encode(persona.getPassword()));
		return repository.save(persona);
	}

	@Override
	public List<Persona> listar() {
		return repository.findAll();
	}

}
