package com.kenyo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * Spring security oauth expone dos endpoints para checkear los tokens
 * /oauth/check_token y /oauth/token_key por defecto es protege denyAll()
 * tokenKeyAccess() y checkTokenAccess() abren los endpoints para ser usados
 * 
 * ClientDetailsServiceConfigurer se usa para definir si se usa para definir la
 * implementacion in-memory o JDBC de los clientes del servicio clientId
 * (requerido) secret (requerido) scope (limitaciones para el cliente)
 * authorizedGrantTypes (tipos de permisos para los cuales está autorizado el
 * cliente) authorities (Autorizaciones que se le da al cliente) redirectUris
 * 
 * @author kenyo
 *
 */

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServer extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private UserApprovalHandler userApprovalHandler;

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()")
				.allowFormAuthenticationForClients();
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory().withClient("personaapp")
		.secret(passwordEncoder.encode("123456"))
		.authorizedGrantTypes("password", "refresh_token", "implicit", "client_credentials",
						"authorization_code")
				// .authorities("ADMIN")
				.scopes("read", "write", "trust")
				.resourceIds("oauth2-resource")
				.redirectUris("http://localhost:8081/login")
				.accessTokenValiditySeconds(5000).refreshTokenValiditySeconds(50000);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore).userApprovalHandler(userApprovalHandler)
				.authenticationManager(authenticationManager);

	}
	

}
